#include "list.h"

int		main(int argc, char **argv)
{
  t_list	list;
  int		i;

  init_list(&list);
  i = 0;
  while (i < 20)
    {
      add_elem(&list, i);
      ++i;
    }
  del_elem(&list, get_elem(&list, 10));
  list.ptr = get_elem(&list, 5);

  for (i = 0; i < 7; ++i)
    printf("%d\n", get_next_elem(&list)->nb);
  printf("\n");

  del_elem(&list, get_elem(&list, 11));
  del_elem(&list, get_elem(&list, 9));

  for (i = 0; i < 4; ++i)
    printf("%d\n", get_prev_elem(&list)->nb);
  printf("\n");

  print_list(&list);
  free_list(&list);
  return (0);
}

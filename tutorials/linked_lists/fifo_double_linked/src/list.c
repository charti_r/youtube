#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void		init_list(t_list *list)
{
  list->nb = 0;
  list->first = NULL;
  list->last = NULL;
  list->ptr = NULL;
}

int		add_elem(t_list *list, int nb)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (-1);
  elem->nb = nb;
  elem->prev = list->last ? list->last : NULL;
  elem->next = NULL;
  ++list->nb;
  if (list->first == NULL)
    list->first = elem;
  if (list->last)
    list->last->next = elem;
  list->last = elem;
  return (0);
}

void		del_elem(t_list *list, t_elem *elem)
{
  t_elem	*tmp;

  tmp = list->first;
  while (tmp)
    {
      if (tmp == elem)
	{
	  if (tmp->prev)
	    tmp->prev->next = tmp->next;
	  if (tmp->next)
	    tmp->next->prev = tmp->prev;
	  if (tmp == list->first)
	    list->first = tmp->next;
	  if (tmp == list->last)
	    list->last = tmp->prev;
	  if (tmp == list->ptr)
	    list->ptr = tmp->prev ? tmp->prev : tmp->next;
	  --list->nb;
	  free(tmp);
	  tmp = NULL;
	}
      else
	tmp = tmp->next;
    }
}

t_elem		*get_elem(t_list *list, int nb)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      if (elem->nb == nb)
	return (elem);
      elem = elem->next;
    }
  return (NULL);
}

t_elem	*get_next_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first && list->ptr->next)
    {
      list->ptr = list->ptr->next;
      return (list->ptr);
    }
  return (NULL);
}

t_elem	*get_prev_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first && list->ptr->prev)
    {
      list->ptr = list->ptr->prev;
      return (list->ptr);
    }
  return (NULL);
}

void	reset_list_ptr(t_list *list)
{
  list->ptr = list->first;
}

void		print_list(t_list *list)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      printf("%d\n", elem->nb);
      elem = elem->next;
    }
}

void		free_list(t_list *list)
{
  t_elem	*prev;
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      prev = elem;
      elem = elem->next;
      free(prev);
    }
  init_list(list);
}

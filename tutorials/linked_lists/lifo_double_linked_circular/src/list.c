#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void		init_list(t_list *list)
{
  list->nb = 0;
  list->first = NULL;
  list->last = NULL;
  list->ptr = NULL;
}

int		add_elem(t_list *list, int nb)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (-1);
  elem->nb = nb;
  elem->prev = list->last ? list->last : elem;
  elem->next = list->first ? list->first : elem;
  ++list->nb;
  if (list->first && list->last)
    {
      list->last->next = elem;
      list->first->prev = elem;
    }
  else
    list->last = elem;
  list->first = elem;
  return (0);
}

void		del_elem(t_list *list, t_elem *elem)
{
  t_elem	*tmp;
  int		i;

  tmp = list->first;
  i = 0;
  while (i < list->nb)
    {
      if (tmp == elem)
	{
	  tmp->prev->next = tmp->next;
	  tmp->next->prev = tmp->prev;
	  --list->nb;
	  if (tmp == list->first)
	    list->first = tmp->next;
	  if (tmp == list->last)
	    list->last = tmp->prev;
	  if (tmp == list->ptr)
	    list->ptr = list->first;
	  free(tmp);
	  i = list->nb;
	}
      else
	{
	  tmp = tmp->next;
	  ++i;
	}
    }
}

t_elem		*get_elem(t_list *list, int nb)
{
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      if (elem->nb == nb)
	return (elem);
      elem = elem->next;
      ++i;
    }
  return (NULL);
}

t_elem	*get_next_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first)
    {
      list->ptr = list->ptr->next;
      return (list->ptr);
    }
  return (NULL);
}

t_elem	*get_prev_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first)
    {
      list->ptr = list->ptr->prev;
      return (list->ptr);
    }
  return (NULL);
}

void	reset_list_ptr(t_list *list)
{
  list->ptr = list->first;
}

void		print_list(t_list *list)
{
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      printf("%d\n", elem->nb);
      elem = elem->next;
      ++i;
    }
}

void		free_list(t_list *list)
{
  t_elem	*prev;
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      prev = elem;
      elem = elem->next;
      free(prev);
      ++i;
    }
  init_list(list);
}

#include "list.h"

int		main(int argc, char **argv)
{
  t_list	list;
  int		i;

  init_list(&list);
  i = 0;
  while (i < 20)
    {
      add_elem(&list, i);
      ++i;
    }
  del_elem(&list, get_elem(&list, 10));
  list.first = get_elem(&list, 5);
  print_list(&list);
  free_list(&list);
  return (0);
}

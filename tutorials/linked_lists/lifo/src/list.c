#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void		init_list(t_list *list)
{
  list->nb = 0;
  list->first = NULL;
}

int		add_elem(t_list *list, int nb)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (-1);
  elem->nb = nb;
  elem->next = list->first;
  ++list->nb;
  list->first = elem;
  return (0);
}

void		del_elem(t_list *list, t_elem *elem)
{
  t_elem	*prev;
  t_elem	*tmp;

  prev = NULL;
  tmp = list->first;
  while (tmp)
    {
      if (tmp == elem)
	{
	  if (prev)
	    prev->next = tmp->next;
	  else
	    list->first = tmp->next;
	  --list->nb;
	  free(tmp);
	  tmp = NULL;
	}
      else
	{
	  prev = tmp;
	  tmp = tmp->next;
	}
    }
}

t_elem		*get_elem(t_list *list, int nb)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      if (elem->nb == nb)
	return (elem);
      elem = elem->next;
    }
  return (NULL);
}

void		print_list(t_list *list)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      printf("%d\n", elem->nb);
      elem = elem->next;
    }
}

void		free_list(t_list *list)
{
  t_elem	*prev;
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      prev = elem;
      elem = elem->next;
      free(prev);
    }
  init_list(list);
}

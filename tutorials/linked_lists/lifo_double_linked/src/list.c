#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void		init_list(t_list *list)
{
  list->nb = 0;
  list->first = NULL;
  list->ptr = NULL;
}

int		add_elem(t_list *list, int nb)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (-1);
  elem->nb = nb;
  elem->prev = NULL;
  elem->next = list->first;
  ++list->nb;
  if (list->first)
    list->first->prev = elem;
  list->first = elem;
  return (0);
}

void		del_elem(t_list *list, t_elem *elem)
{
  t_elem	*tmp;

  tmp = list->first;
  while (tmp)
    {
      if (tmp == elem)
	{
	  if (tmp->prev)
	    tmp->prev->next = tmp->next;
	  if (tmp->next)
	    tmp->next->prev = tmp->prev;
	  if (tmp == list->first)
	    list->first = tmp->next;
	  if (tmp == list->ptr)
	    list->ptr = tmp->prev ? tmp->prev : list->first;
	  --list->nb;
	  free(tmp);
	  tmp = NULL;
	}
      else
	tmp = tmp->next;
    }
}

t_elem		*get_elem(t_list *list, int nb)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      if (elem->nb == nb)
	return (elem);
      elem = elem->next;
    }
  return (NULL);
}

t_elem	*get_next_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first && list->ptr->next)
    {
      list->ptr = list->ptr->next;
      return (list->ptr);
    }
  return (NULL);
}

t_elem	*get_prev_elem(t_list *list)
{
  if (list->ptr == NULL)
    list->ptr = list->first;
  if (list->first && list->ptr->prev)
    {
      list->ptr = list->ptr->prev;
      return (list->ptr);
    }
  return (NULL);
}

void	reset_list_ptr(t_list *list)
{
  list->ptr = list->first;
}

void		print_list(t_list *list)
{
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      printf("%d\n", elem->nb);
      elem = elem->next;
    }
}

void		free_list(t_list *list)
{
  t_elem	*prev;
  t_elem	*elem;

  elem = list->first;
  while (elem)
    {
      prev = elem;
      elem = elem->next;
      free(prev);
    }
  init_list(list);
}

#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void		init_list(t_list *list)
{
  list->nb = 0;
  list->first = NULL;
  list->last = NULL;
}

int		add_elem(t_list *list, int nb)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(*elem))) == NULL)
    return (-1);
  elem->nb = nb;
  elem->next = list->first;
  ++list->nb;
  list->first = elem;
  if (list->last == NULL)
    list->last = elem;
  list->last->next = elem;
  return (0);
}

void		del_elem(t_list *list, t_elem *elem)
{
  t_elem	*prev;
  t_elem	*tmp;
  int		i;

  prev = list->last;
  tmp = list->first;
  i = 0;
  while (i < list->nb)
    {
      if (tmp == elem)
	{
	  prev->next = tmp->next;
	  if (tmp == list->first)
	    list->first = tmp == prev ? NULL : tmp->next;
	  if (tmp == list->last)
	    list->last = tmp == prev ? NULL : prev;
	  --list->nb;
	  free(tmp);
	  i = list->nb;
	}
      else
	{
	  prev = tmp;
	  tmp = tmp->next;
	  ++i;
	}
    }
}

t_elem		*get_elem(t_list *list, int nb)
{
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      if (elem->nb == nb)
	return (elem);
      elem = elem->next;
      ++i;
    }
  return (NULL);
}

void		print_list(t_list *list)
{
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      printf("%d\n", elem->nb);
      elem = elem->next;
      ++i;
    }
}

void		free_list(t_list *list)
{
  t_elem	*prev;
  t_elem	*elem;
  int		i;

  elem = list->first;
  i = 0;
  while (i < list->nb)
    {
      prev = elem;
      elem = elem->next;
      free(prev);
      ++i;
    }
  init_list(list);
}

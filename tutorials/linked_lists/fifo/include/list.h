#ifndef LIST_H_
# define LIST_H_

typedef struct	s_elem
{
  int		nb;
  struct s_elem	*next;
}		t_elem;

typedef struct	s_list
{
  int		nb;
  struct s_elem	*first;
  struct s_elem	*last;
}		t_list;

void	init_list(t_list *list);
int	add_elem(t_list *list, int nb);
void	del_elem(t_list *list, t_elem *elem);
t_elem	*get_elem(t_list *list, int nb);
void	print_list(t_list *list);
void	free_list(t_list *list);

#endif /* !LIST_H_ */

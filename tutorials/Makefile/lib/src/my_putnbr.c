#include "my.h"

static void	__my_putnbr__(int nb)
{
  if (nb >= 10)
    __my_putnbr__(nb / 10);
  my_putchar((nb % 10) + '0');
}

void	my_putnbr(int nb)
{
  if (nb < 0)
    {
      my_putchar('-');
      nb = -nb;
    }
  __my_putnbr__(nb);
}
